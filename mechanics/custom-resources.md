# Custom resources

## ![](../.gitbook/assets/sr\_meds\_large.png) Medical Supplies

The Star Wars galaxy uses different kinds of medical supplies depending on which era you are looking at.&#x20;

In early galactic history, _Kolto_ was the most prominent medical supply. In time, _Juvan_ was discovered and also utilized. Within the new canon books from the High Republic era, the transition between pre-Bacta medical supplies and _Bacta_ usage appears.&#x20;

This led us to agree on **Medical Supplies** as the general term for Kolto, Juvan, and Bacta.

| Usages                                                                                 | Sources                                                                                                                                                                    |
| -------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| <ul><li>Additional Medpacs district</li><li>Healthcare policy</li><li>Events</li></ul> | <ul><li><strong>Base production: 1</strong></li><li>Deposits</li><li>Buildings (Medical Supplies Extractors, Medical Supplies Factories)</li><li>Galactic Market</li></ul> |

## ![](../.gitbook/assets/sr\_heavy\_alloys\_large.png) Heavy Alloys

**Heavy Alloys** can be _Doonium_, _Beskar_, _Nyix_ or other special alloys present in Star Wars that are mostly used for military purposes.

A certain amount of Heavy Alloys is needed to build bigger warships as well as later space station upgrades.

A rare resource with a high price, it is always a bargain if you want to attack the owner to claim it.

| Usages                                                  | Sources                                                                                                            |
| ------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------ |
| <ul><li>Ship sections</li><li>Ship components</li></ul> | <ul><li>Deposits</li><li>Buildings (Heavy Alloy Extractors, Heavy Alloy Forgers)</li><li>Galactic Market</li></ul> |

## ![](../.gitbook/assets/sr\_spice\_large.png) Spice

The main resource for criminal empires throughout the galaxy.&#x20;

These criminal empires will use **Spice** to build and spread their branch offices.&#x20;

Non-criminal empires can use Spice for bribing criminals or improving their relations with criminal cartels as well as to refine it into **Medical Supplies**.

| Usages                                              | Sources                                                                                                                                            |
| --------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
| <ul><li>Decisions</li><li>Ship components</li></ul> | <ul><li>Deposits</li><li>Buildings (Spice Extraction Mines, Synthetic Spice Refineries, Criminal Branch Offices)</li><li>Galactic Market</li></ul> |

## ![](../.gitbook/assets/fuel\_large.png) Starship Fuel

Building a fleet is easy, maintaining it might actually be harder.

Fuel is consumed by every ship inside your empire from Science vessels up to Dreadnoughts. You can acquire it in the early game by expanding your empire or try to synthesize it later on.

Just like with other resources, we decided on a generic name to enable events which reference different fuel types like _Coaxium_.

| Usages                         | Sources                                                                                                                                                            |
| ------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| <ul><li>Ships upkeep</li></ul> | <ul><li><strong>Base production: 5</strong></li><li>Deposits</li><li>Buildings (Fuel Extraction Pump, Synthetic Fuel Refineries)</li><li>Galactic Market</li></ul> |

## ![](../.gitbook/assets/crew\_large.png) Manpower

Another important part of maintaining a strong armada is **Manpower**. Every ship and army needs Manpower.

Since LotOR starts relatively early in galactic history, a lot of manpower is needed for ships and stations. However, these requirements will go down in time, by researching special techs and using special components.

| Usages                                                                                        | Sources                                                                                                                                                                                              |
| --------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| <ul><li>Building ships and ships upkeep</li><li>Recruiting armies and armies upkeep</li></ul> | <ul><li><strong>Base production: 10</strong></li><li>Crew buildings (Military Academies, Training Centers)</li><li>Recruitment Policy</li><li>Cloning Technologies</li><li>Galactic Market</li></ul> |
