---
description: A running list of frequently ask questions and their answers.
---

# Frequently Asked Questions

## General

<details>

<summary>What is LotOR? How is it different from New Dawn?</summary>

Legacy of the Old Republic is a total conversion set in the Old Republic era, starting from 3996 BBY at the end of the Great Sith War. Many mechanics are more akin to a vanilla Stellaris playthrough and Star Trek: New Civilizations.

</details>

<details>

<summary>The mod isn’t downloading/updating? What should I do?</summary>

If you’re having issues with the mod not updating, unsubscribe and resubscribe.

If you’re having issues downloading, you may need to delete the mod folder from Documents\Paradox Interactive\Stellaris.

If you need help with this, ask for help on our [Discord](https://discord.gg/4xfQ78sPpm) server.

</details>

<details>

<summary>Which DLCs do I need/are recommended?</summary>

No DLC is needed. However, the following ones are recommended for additional features:

* Nemesis
* MegaCorp
* Federations
* Synthetic Dawn (for Killiks)
* First Contact (Cloaking)

</details>

<details>

<summary>Does LotOR work with [insert mod here]?</summary>

Here is what LOTOR works with without a problem:

* Music mods
* Tiny Outliner & Fleets (above lotor)
* Custom portraits, flags, name lists, shipsets (Mostly cosmetic things that do not use any vanilla technologies)
* Most custom civics
* Some custom traits

Custom origins might not work properly, and custom initializers will not work.

**We will not be providing support for larger overhaul mods such as NSC, Gigastructures, etc. It is completely up to those mod makers or other modders to make and release a compatibility patch for LotOR.**

</details>

<details>

<summary>Where can I see a list of available patches?</summary>

[updates.md](updates.md "mention")

</details>

<details>

<summary>Will you release the shipsets as standalone?</summary>

With the public 1.0 release, we are not planning to release any of the ships as standalone. Some shipsets are not finalized, and others are still in development, and we do not grant anyone permission to release the shipsets individually.

Once the shipsets are properly fleshed out, we may consider releasing them as standalone mods.

</details>

<details>

<summary>Where is X? Where is Y? Where is Zakuul?</summary>

Our 1.0 release focuses on events leading up to the Jedi Civil War. We are in the process of fixing, expanding, and improving content for the release of Overlord. Once Overlord is released, and our mod is compatible with it, we will begin proper work for KOTOR content and fleshing out other empires.

With that being said, the Eternal Empire is not implemented, and we currently don't have any plans for it to be implemented for quite some time.

</details>

## Mechanics

<details>

<summary>My custom empire doesn't work when I load in. I thought LotOR was supposed to support custom empires?</summary>

When selecting the traits for your custom empire species, there are a series of traits to choose from that cost 0 points; these traits determine where your empire will spawn in the galaxy. Please note that multiple custom empires cannot select the same spawn location.

</details>

<details>

<summary>I’ve edited an existing empire, but the changes didn’t stay when I loaded in the game. Why is that?</summary>

Some empires have leaders, traditions, and origins that are tied to events and other functions in the mod. To ensure it doesn’t break, we force some things to remain constant.

</details>

<details>

<summary>Why can’t I access some solar systems?</summary>

Some systems are intentionally cut off for story reasons. Hyperlanes will be open to them as the game progresses. Others will require you to research technologies to establish hyperlanes between certain sections of the galaxy.

</details>

<details>

<summary>Why isn’t the Republic playable as its own faction?</summary>

Unlike the Galactic Empire or the United Federation of Planets in Star Trek, the Galactic Republic is not a cohesive collection of worlds and people that share a common goal. In both Canon and Legends, the member worlds of the Republic are at odds with each other for some reason or another and each of the member worlds - while wanting to preserve the Republic - still pursue their own aims. The best way to capture this with the mod is by having the four core members of the Republic (Alderaan, Coruscant, Corellia, and Duro) act as their own empires.

</details>

<details>

<summary>Will the Republic ever be its own playable faction?</summary>

Not in the near future.

</details>

<details>

<summary>I’m a member of the Republic, and when I zoom out, my borders change to orange. Is that a bug?</summary>

Nope! We wanted the Galactic Republic members to be more easily noticeable at a glance without clicking through menus.

</details>

<details>

<summary>I can’t see my empire’s name when I zoom out. Why is that?</summary>

In vanilla Stellaris, there is a bug where an empire’s name can take up a massive portion of the galaxy map for no real reason. We have chosen to remove empire names from the galaxy map to prevent this problem from occurring.

</details>

<details>

<summary>The Sith Empire and Mandalorians are listed as crisis but also playable. How does that work?</summary>

If no one is playing either empire, the AI will automatically follow a path that leads them to become crisis events. If a player is in control of either empire, then it will be up to them to follow events and traditions that set them on the path to being the crisis **without using the “Become the Crisis” perk from Nemesis**.

</details>

## Content

<details>

<summary>Will [insert faction here] get their own ship set?</summary>

Some empires will be getting their ships in future content updates. Other empires won’t be receiving a unique ship set; due to a lack of designs in both Canon and Legends material and no real point of reference for designs.

</details>

<details>

<summary>Why does the Sith Empire use the Supremacy as their juggernaut?</summary>

Some Stellaris features that were adapted for LotOR are technically outside of the Lore. The Supremacy acts as a stop-gap in the Sith ship set for their juggernaut since we do not currently have one designated for them. We might address this in the future, but it's a low priority for now. You can disable these additional features by setting the Extended Feature option to false when you set up your galaxy.

</details>

<details>

<summary>Is [insert ship class here] in the mod?</summary>

There are a few determining factors when it comes to including a ship in our mod: does a model for it exists, do we have permission to use it, and does it work within the context of our mod.

Not all the ships we have models for are in the mod yet. Some ships, such as the Leviathan-class, will be included alongside the eventual KOTOR update as we expand our Republic and Sith ship rosters.

</details>

<details>

<summary>Why is the Foray-class upside down?</summary>

Our glorious leader Bane is utterly convinced that it must be positioned this way, and this is the hill he will die on.

</details>

<details>

<summary>Why is Starlight Beacon used as the menu screen? I thought this was supposed to be in the Old Republic era?</summary>

Because pretty station looks pretty. Your objections have been ignored; please don't ask for the pretty station to be removed. It would make us very sad.

</details>

## Lore

<details>

<summary>How accurate is the mod to Legends material?</summary>

Stellaris does not allow for a 1:1 recreation of events from Legends or 1:1 adaptations of anything. We had to be creative with how we would have events play out in this mod and figure out the best way to adapt elements from Legends and Canon together. Some story elements from the various comics, games, and novels don’t adapt well to this type of strategy game.

In short, we are focusing on gameplay over strict adherence to Legends/Canon material.

</details>

<details>

<summary>Why are you merging Canon and Legends material together?</summary>

The focus of this mod is mainly on Legends material. Aside from a few vague references to certain parts of this era (Qel-Droma Epics, Revan, HK series assassin droids, unnamed devastation to Taris, to name a few), the Old Republic era is relatively unexplored in the current canon.

Most of the canon stuff we are including is reasonably minimal. There are a handful of references sprinkled throughout this mod. And as far as containing significant High Republic era content like the Nihil, they won’t be seen in the mod for some time.

If including the smallest amount of canon content in the mod bothers you, this is not the mod for you.

</details>

<details>

<summary>Will you make a version with only Legends material?</summary>

No.

</details>
