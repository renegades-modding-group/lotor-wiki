# Table of contents

* [Introduction](README.md)
* [Frequently Asked Questions](frequently-asked-questions.md)
* [Updates](updates.md)

## 〽 Getting started

* [Game settings](getting-started/game-settings.md)
* [Custom empires](getting-started/custom-empires.md)
* [Compatibility with other mods](getting-started/compatibility-with-other-mods.md)

## 🔧 Mechanics

* [Galactic Republic](mechanics/galactic-republic.md)
* [Custom resources](mechanics/custom-resources.md)

## 💫 Empires

* [Core Region](empires/core-region/README.md)
  * [Coruscantii Union](empires/core-region/coruscantii-union.md)
* [Northern Dependencies](empires/northern-dependencies.md)
* [Hydian Borderlands](empires/hydian-borderlands/README.md)
  * [Mandalorian Space](empires/hydian-borderlands/mandalorian-space.md)
  * [Remnants of the Sith Empire](empires/hydian-borderlands/remnants-of-the-sith-empire.md)
* [Slice Region](empires/slice-region/README.md)
  * [Theocracy of Onderon](empires/slice-region/theocracy-of-onderon.md)
  * [Hutt Empire](empires/slice-region/hutt-empire.md)
* [New Territories](empires/new-territories.md)
* [Trailing Sectors and Western Reaches](empires/trailing-sectors-and-western-reaches.md)
* [Unknown Regions](empires/unknown-regions.md)
