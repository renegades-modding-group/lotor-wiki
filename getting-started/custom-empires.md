# Custom empires

## About

LotOR allows you to create your own custom empire and spawn in predefined locations around the galaxy. This feature can be combined with third-party portrait and shipset mods to create your own narrative.

It's important to note that not all vanilla origins will work with this feature so if you intend to use one of the crazy ones make sure to check with the community on Discord if it's supported. Also, these predefined locations are limited in number so only a certain amount of custom empires can spawn in at the same time.

## How it works

When you create your empire you must pick one of the 0 cost traits depending on the sectors where you want to spawn. You can find more information about the available sectors and the traits below.

## Spawn locations

<figure><img src="../.gitbook/assets/custom empires spawn locations.png" alt=""><figcaption></figcaption></figure>

## Traits

{% hint style="warning" %}
#### **Multiplayer**

Make sure there isn't another player empire with the same trait
{% endhint %}

{% hint style="danger" %}
Do **NOT** use for Necrophage Prepant Species
{% endhint %}

<table><thead><tr><th width="290.3333333333333">Trait</th><th width="269">Description</th><th>Notes</th></tr></thead><tbody><tr><td><img src="../.gitbook/assets/trait_spawn_southern_core.png" alt=""> Southern Core Spawn</td><td>An empires with this trait will spawn in the Dentaal system, located in the Southern Core.</td><td>—</td></tr><tr><td><img src="../.gitbook/assets/trait_spawn_hydian_borderlands.png" alt=""> Hydian Borderlands Spawn</td><td>An empires with this trait will spawn in the Ampliquen system, located in the Hydian Borderlands.</td><td>—</td></tr><tr><td><img src="../.gitbook/assets/trait_spawn_slice.png" alt=""> Slice Spawn</td><td>An empires with this trait will spawn in the Nanth'ri system, located in the Slice.</td><td>—</td></tr><tr><td><img src="../.gitbook/assets/trait_spawn_new_territories.png" alt=""> New Territories Spawn</td><td>An empires with this trait will spawn in the Ajan Kloss system, located in the New Territories.</td><td>—</td></tr><tr><td><img src="../.gitbook/assets/trait_spawn_trailing_sectors.png" alt=""> Trailing Sectors Spawn</td><td>An empires with this trait will spawn in the Stend system, located in the Trailing Sectors.</td><td>—</td></tr><tr><td><img src="../.gitbook/assets/trait_spawn_western_reaches.png" alt=""> Western Reaches Spawn</td><td>An empires with this trait will spawn in the Ione system, located in the Western Reaches.</td><td>—</td></tr><tr><td><img src="../.gitbook/assets/trait_spawn_unknown_regions.png" alt=""> Unknown Regions Spawn</td><td>An empires with this trait will spawn in the Firrerre system, located in the Unknown Regions.</td><td>Not available on the small map.</td></tr></tbody></table>
