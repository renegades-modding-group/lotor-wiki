---
description: >-
  This page contains all the changes that have been made to Star Wars: Legacy of
  the Old Republic since its 8th April 2022 release.
---

# Updates

## Foundation (1.0.0)

Foundation is the initial LotOR release and is focusing heavily on the Mandalorian Wars, the build up to the Jedi Civil War with the foundation of the Galactic Republic. (test MR - remove after) Foundation is the initial LotOR release and is focusing heavily on the Mandalorian Wars, the build up to the Jedi Civil War with the foundation of the Galactic Republic. (test MR - remove after)

### Version 1.3.5a (2024.05.31)

{% code overflow="wrap" %}
```yaml
3.12.4 Compatibility, normally those updates are savegame compatible, but many things changed in vanilla + lotor, a new game is strongly recommended.

Reworks & Additons:
- Completely Reworked Enclave Stations, they can now spawn Dynamially over the entire Galaxy and got replaced by lore accurate Companies/Groups
- Completely Reworked:
- Sith Ziost Event Chain
- Pacification of Dromund Kaas Event Chain
- Wastelands of Kalsunor Event Chain
- Reworked Sith Armies to be more lore friendly (new icons)
- Added a new number of events to the Sith reveal to the Galaxy
- Slightly reworked the Dromund Kalakar Shipyards Event
- Slightly reworked two more early Sith Events
- Reworked Parts of the Map (North Part of the Galaxy)
- Switched all HoloNet technologies from Physics to Society

Changes:
- Added tooltips to all Manufacturer Tradition Perks & Starbase Buildings
- Added and changed many Event Pictures
- Removed Unknown Regions Minor Powers from Start Screen
- Improved some Deposit Descriptions
- Added dismantle options to lotor megastructures
- created a new holonet communication technology which is accessible to sith & unknown regions to allow them to get communication relays

Fixes:
- added a missing effect to the Starship Engineering Tradition
- fixed a bug with force sensitivity, where Grand Moff Vaiken could get it
- fixed a bug with force sensitivity where it was pickable
- fixed some issues with sith hyperlanes upon reveal
- fixed a bug where the Sith Empire couldnt lose the Devastated Shipyards modifier
- fixed a bug with Korribans Planet background
- fixed a issue causing countries to lose access to market resources
- disabled main menu machine age background noise
- disabled colony portal events
- disabled the Prince Anomaly
- added a failsafe incase the republic situation gets stuck
- fixed kilik tradition having a wrong effect
```
{% endcode %}

### Version 1.3.5 (2024.05.17)

{% code overflow="wrap" %}
```yaml
- 3.12.2 Compatibility Update

Major System Changes:
- Removed Resources Heavy Alloys, Isotope-5; both never really worked out, instead they are now folded into the new special resource system which includes several metals, gemstones, medical substances and more (roughly 40)
- Reworked all unique alloy deposits as well as multiple other deposits into the new special resources system
- Rebalanced Ship build time and cost

Major Map Changes:
- Created a dynamic spawning system for pirates, space animals and more
- Layout reworks around the Outer Rim, Hutt Region and more, includes new ancient and nebulous hyperlanes as well as a new background to decrease the viewability on the galactic map
- randomized more less lore relevant worlds for better replayability
- created a random spawning system for Space Critters
- hid unsurveyd solar systems

New Ascension Perks:
- Base Delta Zero Protocol (Unlocks Armageddon Bombardment)
- Hyperspace Mastery (Increased Hyperspace speed, an early unlock for next technology)
- Droidsmith Specialization (Increased Droid research speed, increased droid assembly speed & decreased cost)
- Imperial Resilience (Empire Area Defensive Buffs, +2 loyalty from subjects)
- Pioneering Warship Construction (Increased Ship research speed, early unlock for next shipsize)

New Tradition:
- Ship Engineering, about manufacturing starships, manufacturers like Kuat and orbital shipyard megastructures, a dozen empires have unique perks in it like Kuat having one about Kuat Drive Yards & their special warships...
- Reworked all Manufacturer Starbase Modules, locked them behind the Ship Engineering tradition
- Reworked Supremacy Tradition Master Shipwrights & subtitution perks

Major Reworks:
- Reworked large part of Droid Technologies from Physics -> Engineering
- Added a new unique Research Category for Droid technologies

Empire Changes:
- Core Worlds:
-- Added Unique Governments & Ruler Titles to the Republic Founding Empires
- Guardians of the Whills:
-- Changed Government Megacorp -> Oligarchic Holy Tribunal
-- Changed Civic Effects & Civic
-- Reworked Homeworld Deposits
- Bothan:
-- Changed Species: + Talented + Communal + Quarrelsome
- Zabrak:
-- Changed Species: + Strong + Resilient
- Rodia:
-- Added Homeworld Deposits
-- Changed Species: + Nomadic
-- Changed Civic Effects
- Pyke:
-- Changed Species: + Quick learners - Decadent
- Falleen:
-- Changed Species: + Venerable + Solitary + Wasteful
- Killik:
-- Changed Species: + Rapid Breeders + Fleeting
- Hutt Empire:
-- Changed Hutt Species: Deviant --> Repugnant
-- Tweaked Hutt Civic
- Trandosha:
-- Reworked Homeworld Deposits
- Alskaan:
-- Reworked Homeworld Deposits
- Mandalore:
-- Reworked Homeworld Deposits
- Dorin:
-- Changed Civic Efficient Burreaucracy -> Ascensionists
-- Changed Kel Dor Species: Completely changed traits and added a unique Antiox Breathing Mask trait
-- Changed Homeworld location based around the lore
- Ahto Council:
-- Reworked Ahto Councilor Effect
-- Reworked Homeworld Deposits
- Malastare:
-- Reworked Malastare Councilor Effect
- Muun:
-- Changed Species: + Intelligent + Enduring + Slow Breeders - Sedentary
-- New Muun Portraits with adjusted Clothing
- Ithorian Herds:
-- Changed Ithor Species: Remade
-- Reworked Homeworld Deposits
- Guardians of the Whills:
-- Reworked Homeworld Deposits
- Chiss:
-- Changed Chiss Species: + Talented
-- Reworked Homeworld Deposits
- Ryloth:
-- Added Homeworld Deposit

Other Changes:
- Shrimp AI Mod Compatibility
- reworked Droid Modding Technology Desciptions + Icons
- reworked some Criminal Industries Tradition Perks
- reworked Killik Hivemind Tradition completely now offers early start to Ascension Perk - Genetic Assembly
- reworked The Legend of the Mythosaur Tradition Perk
- reworked Values for multiple Armor Components
- reworked Emnity & Aptitude tradition icons
- reworked Quantum-crystalline Armor
- Heavy & Oridium Armor are now dependant on having one of the deposits in your empire
- Droid technologies reworked to match canon droid class designations
- Torpedos, Missiles, and Shield Types all have a weaker version on game start
- improved multiple lotor custom edicts with better effects and descriptions
- Republic Member Worlds no longer suffer from divided loyalty modifiers
- updated multiple technology icons
- removed some rather useless technologies for less bloat
- nerfed all animal deposit ranger job bonuses
- changed some effects on the Sith Espionage Tradition
- improved Missile Component & Technology Icons
- improved some lotor paragon event screens
- improved Drengir and Pirate ship loadouts
- added multiple new Droid Portraits
- added line combat role to Corvettes
- added starbase combat role to all stations
- added new descriptions to various auxiliary components
- added dynamically spawning starwars pirate fleets to the galaxy

Fixes:
- adjusted certain hat sizes for human like species
- various localisation fixes
- fixed defensive stations not having access to shield type and crew
- Fuel and Enforcer technology's research tier progression adjusted to be in line with other job improvement technologies
- fixed Zabrak being set to slaves for Iridonian Sector players
- fixed Torpedo slots for all cruiser and larger ship sizes can now equip long range torpedo tubes
- fixed missing districts on the Space City habitat world
As usual these are only the most importnat additions and fixes!
```
{% endcode %}

### Version 1.3.4a (2024.04.11)

{% code overflow="wrap" %}
```yaml
- 3.11.3 Compatibility
```
{% endcode %}

### Version 1.3.4 (2024.03.21)

{% code overflow="wrap" %}
```yaml
Changes:
- 3.11.2 Compatibility Update
- Completely Reworked HoloNet Technologies (effects, icons, desc) & added several new ones (10ish)
- Reworked almost all Swoop Racing Events with new omages, effects, localisation
- Reworked all Droid Brain Technologies (effects, icons, desc) & added several new subtechnologies (5ish)
- Reworked Czerka Tradition Tree to be a Mercantile swap, buffed Czerka Civic
- Reworked Verpine Hive Civic from Verpine Technologies -> Cutting-Edge Engineers with new desc & effects
- Reworked Manaan Homeworld Deposits & Origin Effect
- Rebalanced Communciation Relays (Megastructure)

Additions:
- Added new playable Empire: Neimoidian Purse Worlds, a fanatic materialistic and authoritarian empire inside the core worlds
- Tweaked Galactic Layout for Neimodian Worlds Cluster
- Added multiple new Animal Deposits & Neimoidian Homeworld Deposits
- Improved the Echani Command with: A new Civic - Combat Driven Society, and new Homeworld Deposits
- Added a new "Quesh Venom" anomaly
- Created a new Flavor tie in Event for the HoloNet
- Added new Primitives: Nautolan Elder Council on Glee Anselm
- Added Honoghr to Small & Medium Map
- Klatooinian portraits from New Dawn
- Reworked Togruta portraits - thanks to Walshicus for permission to use the new mesh for both Togruta and Nautolans
- Prarie world subclass for certain worlds

Fixes:
- various localisation fixes
- fixed weird layout of Glorious Hutt Empire - Tradition
- fixed human military service for Sith Empire
- fixed subterranean & lithoid origins
- fixed Republic first contact events no longer use Sith related music
- fixed another way of Coruscant staying a vassal
```
{% endcode %}

### Version 1.3.3 (2024.03.03)

{% code overflow="wrap" %}
```yaml
Fixes:
- minor local issue in Anomaly Event
- fixed Deep Space Hyperlanes not working
```
{% endcode %}

### Version 1.3.3 (2024.03.02)

{% code overflow="wrap" %}
```yaml
Major Changes:
- Reworked all combat behavior with help from ACG & STNC, battles are now alot more Star Wars like than before
- Added additional combat roles to Dreadnoughts, Battlecruisers, Battleships
- Completely Reworked the Namelists for Mandalorians, Sith, the Republic
- Reworked the Sentient Property Tradition with new effects
- Reworked the Galactic Market into the Galactic Trading Network, which is open to all Empires (except the Unknown Regions Empires + the Sith as long as they are hiding), you can now freely trade goods and slaves galaxy wide after it is established roughly 20 years into the game!
- The Sith Remnant now start with 24 Sith Pops & 8 Human Pops
- Changed the Sith Species Traits: Unique - Sith Pureblood, Unique - Sith Alchemy
- Changed the Wookie Species Traits: Unique - Kashyyyk Resiliance
- Aligned numerous Technologies & Buffs/Upkeep Stuff with Vanilla, e.g. removed alot of less Ship production cost modifiers
- Reworked some Techs with new Effects, Weights, and Cost
- Kuat now starts with the Ruthless Competition Civic isntead of Private Prospectors
- Czerka now starts with the Indentured Assets Civic instead of Ruthless Competition
- Large Spektrum laser damage buffed to be in-line with Turbolasers
- Toxic Waste Dispensers shield and armor penetration reduced to 50 %
- Changed Hutt Glorius Empire Tradition around, reworked localisations

Additions:
- Added Event Chain: Crime Lords of Nal Hutta (by Kaimur)
- Added 2 new animal deposits to the galaxy
- Rebalanced various Deposits
- Improved the localisation of almost all deposits
- Added Carida System Initializer

Fixes:
- Fixed Sentient Property Tradition always giving Rodian Slaves
- science ships can now cloak again (if you have the dlc + ascension perk)
- Alsakan can now get their Hyperlane with Utrost (Alsakan - Coruscant connection)
- Korriban's planet view fixed
- Fixed several minor tradition problems
- Fixed weapon tags to work with admiral traits, techs, and edicts
```
{% endcode %}

### Version 1.3.2a (2024.02.11)

{% code overflow="wrap" %}
```yaml
Fixes:
- disabled dark hair for Echani
- fix for two edge cases where Sith player could get stuck in the Conspiracy chain
- fixes broken deep space hyperlanes
- fixes potential issue with ai weight bug in certain traditions
```
{% endcode %}

### Version 1.3.2 (2024.02.07)

{% code overflow="wrap" %}
```yaml
Changes:
- Rebalanced almost all Event Rewards, Please give us feedback on this, since its a rather dramatic rework. Hence if you feel like any events have to little or to much reward make a suggestion post.
- Reworked 5 old lotor Anomalies with new rewards, options, and texts
- Now removes the Guided by the Dark/Light Side of the Force trait once a leader becomes a proper Jedi/Sith
- Removed effects from Sub-Planet Types; we couldnt figure out a way for them to make snese and be fun atm.
- Torpedo tube power costs reduced
- Slightly reworked various technologies & improved their effects as well as ai weights
- Deep Space Hyperlanes now require a initial exploration after the technology was researched

Additions:
- Added the Argo - Class Heavy Cruiser, a dedicated carrier for the open market
- Added two new Force Classes: Jedi Liaison & Sith Advisor (similar classes if you want your Jedi/Sith to support your empire from the Council)
- Rebalanced other Jedi & Sith Traits
- Emperor Vitiate's Sith Sorcerer trait replaced with new unique Sith Lord trait that buffs Rulers.  (New game required)
- Added some new Deposits & related Events
- Galactic Community Members now have access to the Republic Ships

Fixes:
- Fixed conflicting planet modifiers from galaxy generation
- Fixed Advozse Hegemony and Tayan League capitals having the wrong name if played by AI
- Fixed Escape Pods technology not working
- Fixed the Ai not using Lotor unique councilors
- Fixed Hutt Ground Commander Trait not existing
- Fixed an Issue where the AI could get stuck on building certain armies
- Fixed wrong component costs for several larger shipclasses (Astromechs, Tractorbeams, ...)
- Fixed AI ship weapon loadouts to stop them from spamming ion cannons and beam lasers
- Fixed Republic member worlds anexing other empires
```
{% endcode %}

### Version 1.3.1b (2024.01.19)

{% code overflow="wrap" %}
```yaml
Changes:
- Changed the Iridonian Empire to be Militarist, Spiritualist, Xenophobe
- Tweaked their Civic to be more about their Territory Defense, updated effect
- Changed the Twilek Enclave to be Egalitarian, Xenophile, Pacifist
- Tweaked their Civic to be more about their Adaptivity to Nature, updated effect
- Changed the Rodian Realm to be Authoritarian, Militarist, Xenophobe
- Tweaked their Civic to be more about Planetary Defense & Stability
- Changed the SoroSuub Corporation to be Fanatic Materialists, Xenophile
- Tweaked their Councelor to be the Chief Cosmetologist
- Changed the Chiss Ascendancy to be Authoritarian, Xenophobe, Militarist

- Rebalanced most weapon cost: especially for strategic resources (Volatile Materials, Exotic Gases, Rare Crystals)
- Reworked Beam Cannons: Very strong against armor and hull but very weak against shields; added Large Variant
- Reworked Rapid Fire Laser Cannons: Very fast fire rate, slightly strong against shields and hull, very weak against armor

Fixes:
- fixed issue with Radiation missiles
- fixed Vanilla Great Herald Digsite appearing
- fixed almost all weapons having wrong upkeep values
```
{% endcode %}

### Version 1.3.1 (2024.01.15)

{% code overflow="wrap" %}
```yaml
Changes:
- Larger rework to the Galactic Republic Tradition & some fixes
- Tweaked Empire Megastructure Capacities
- Added Leader Species Weights to Elections (It is now harder for non main species leaders to be elected as the ruler)
- Tweaked Several Designs & Loadouts for Hutt ships:
  Terrada Cruiser, Kadijic Heavy Cruiser, Kopraja Battlship, Po'runga Battlecruiser, Dor'bulla Dreadnought
- Humans are now always Full Citizens inside the Sith Empire
- New model for the Dor'bulla Dreadnought
- New generic ruler uniform for new_human and humanoid_01 portraits
- Complete Rework of Animal Deposits
- New Wildlife Control Policy
- Rebalanced all Animal Deposit Values
- Refactored all planetary modifiers to be more in line with each other
- Background structure tweaking for easier updates
- Sith Deriphan Cruiser Model Update
- Refactored Crew resource generation
- Refactored Army file structure, updated & aligned costs, build time, Ai weights
- Rebalanced all army costs, build time
- Weapon cost tweaks:
  - Mass drivers are cheaper 
  - Turbolasers are slightly more expensive
  - Torpedoes cost scaling normalized (higher tiers will be noticeably cheaper)
  - X slot weapons are more expensive
- Army stats tweaked to be consistent with costs and design goals
- Reworked the Tradition Legacy of Xim for the Tion Hegemony into:
  - The Hegemony (Domination Tradition Swap)
  - Legacy of Xim (Unyielding Tradition Swap)

Fixes:
- Sith ship designer fix
- Several typos fixed
- Fixed Corellia AI choosing Republic ships instead of their new shipset
- Fixed numerous discrepancies with ship descriptions and loadout templates
- Fixed fleet composition text icons displayed as "?"
- Added Elections button back to Government UI
- Fixed long range torpedo tubes missing on many ship classes
- Fixed a serious AI issue which led them to produce thousands of tons of fuel while disregarding everything else
- Added numerous missing localisations & improved planetary modifier ones
- Size Damage Multiplier removed from XL weapons - no more one shot kills on dreadnoughts
- Fixed some Tradition effects not working for the Duro
```
{% endcode %}

### Version 1.3.0 (2023.12.21)

{% code overflow="wrap" %}
```yaml
Sith Rework:
- New Situation for the Sith Empire
- Starts on year 2 and has 4 Stages:
  - Stage 1: Rebuilding an Empire
  - Stage 2: Consolidate our Realm
  - Stage 3: Preparations
  - Stage 4: Territory Conquest
- The Sith start fully boxed in, without any access to outside areas.
  Each time the situation advances to a new stage, one or more hyperlanes will open up.
- All Sith story events are now part of the situation, making it advance faster in early stages.
  Most existing events will happen in Stages 1 and 2, with only a few sometimes happening in Stage 3.
- New 'Conspiracy' event chain by Bosmer added in Stage 3.
- Once reaching stage 3, aggressive actions will help advance the situation, making the Crisis start sooner.
  Aggressive actions are the same as used in the Mandalorian crisis, plus destroying enemy ships.
- Large stability boost to Sith AI during the 3 initial stages, to prevent early to mid-game uprisings.
- The Sith will only have access to Navigation technologies in Stage 4.
- During Stage 4, the player (not AI) will have the option to reveal themselves via special project, starting the Crisis sooner.
- The Situation will end around 2335, starting the crisis automatically, unless the player started it earlier via special project.
- Nothing has been changed related to the crisis itself (for now).

New feature: Blueprint Selector
- New button in Ship Designer menu opens the blueprint selector menu.
- Ship designs can be replaced for an energy credit cost. Can still only use one design per ship size at a time.
- One year cooldown between replacing ship designs.

Changes:
- Refugees will now always arrive on Dromund Kaas, instead of a random colony.
- Vitiate's shuttle has been removed. The quest chain remains unchanged, but works on a timer.
- Sith now have access to Armageddon orbital bombardment upon reaching Stage 3 of the situation.
- Reinstated system Orbital Ring capacity limit.
- Improved some Tradition descriptions and effects.
- Updated various Civics with new effects & texts.
- Legion Corvette, Raider Frigate, and Terminus Cruiser have been slightly buffed and are now modern designs unlocked via research and special project.
- Updated Hutt Terrada Cruiser & Kadijic Heavy Cruiser.
- Updated and aligned all message icons.
- Various improvements to event texts & localisation.

New:
- Added Kaleesh, Yamrii, Nagai and Squib pre-ftls.
- New unique deposits added to Korriban.
- New Klatooinian portraits.
- New generic ships added: Admonish Cruiser, Despot Cruiser, and Usurper Heavy Cruiser.
- Added Advozse Hegemony: a xenophobic, militarist and materialist empire in the southern parts of the galaxy. Inhabited by the territorial Advozec.
- Added Tayan League: a pacifist, fanatic equalitarian empire in the southern parts of the galaxy. Inhabited by the peaceful and socialist Gran.
- Updated Hammerhead Cruiser with a new model, new loadouts and a potential carrier refit.
- Added Corellian Shipset: Civilian ships, fighters, and 11 new warship classes.
  Kuat ships and the new Corellian ships require good relations with the owner of Kuat or Corellia to obtain.
- Added 3 new Sith ships: Chrysalide Corvette, Devourer Frigate, and Derriphan Cruiser.
- Three tracks from the "Music of the Old Republic Era" mod.

Fixes:
- Massassi pops will no longer become livestock on being enslaved by event.
- Fixed multiple issues with Origins not showing their effect.
- Fixed Pathovian Cluster - Florn Deepspace Hyperlane.
- Resolution for Member Worlds no longer applies to Subsidiaries.
- Fixed multiple rooms not showing up.
- Changed duplicate icon for "Destabilize" tradition perk.
```
{% endcode %}

### Version 1.2.7h (2023.12.14)

{% code overflow="wrap" %}
```yaml
Stellaris 3.10.4 Compatibility update

Changes:
- Removed temporary fix to make AI hire scientists (fixed in vanilla)
```
{% endcode %}

### Version 1.2.7g (2023.12.08)

{% code overflow="wrap" %}
```yaml
Fixes: 
- Starting as the Killik Hive will now longer crash the game
```
{% endcode %}

### Version 1.2.7f (2023.12.07)

{% code overflow="wrap" %}
```yaml
Stellaris 3.10.3 Compatibility update

Changes:
- Master Builders AP now affects Lotor megastructures
- Dorin System changed to dual Black Hole
- New starting screen text for the Dorin League by Query
 
Fixes: 
- Job weights tweak to prevent droids from stealing specialist jobs
- Supreme Chancellor trait is now working again, got broken with 3.10
- Sith will no longer receive the Mandalorian Crusade starting event
- Corrected Praetorian Gunship loadout to match description

Temporary Fixes:
- AI will now hire scientists if needed on leader pool refresh
- Adjusted leader pool fix from 1.2.7c to correct base value
```
{% endcode %}

### Version 1.2.7e (2023.11.29)

{% code overflow="wrap" %}
```yaml
Stellaris 3.10.2 Compatibility update

Changes:
- Disabled vanilla anomaly Intemporal Orb

Fixes: 
- Unknown Regions empires should now get navigation techs correctly
- Adjusted leader pool fix from 1.2.7c to account for external leaders
- Skeleton Crew component will no longaer have negative regen
- Sith 'Path to Immortality' chain no longer bypasses final event (need new game if past it)
- Hyper Relays should now be upgradeable in all maps, if on a major Trade Route
```
{% endcode %}

### Version 1.2.7d (2023.11.25)

{% code overflow="wrap" %}
```yaml
Fixes:
- Fix for CTD when country destroyed while involved in other wars
```
{% endcode %}

### Version 1.2.7c (2023.11.25)

{% code overflow="wrap" %}
```yaml
Fixes:
- Fix for leader pool overflow causing a CTD on some not-so-rare cases.
```
{% endcode %}

### Version 1.2.7b (2023.11.23)

{% code overflow="wrap" %}
```yaml
Stellaris 3.10.1 Compatibility update

New:
- Added Demolish button to Ruined Communications Array and Ruined Hyper relay megastructures

Fixes:
- Added Relic World to planet types not allowed for Selective Terraforming 
- Dark Side trait now applies to self
- Disabled a bunch of unnecessary vanilla logs
```
{% endcode %}

### Version 1.2.7a (2023.11.18)

{% code overflow="wrap" %}
```yaml
Fixes:
- Added missing vanilla planet type icons that were causing offset issues
- Disabled a couple of astral planes traits
- Planet survey events should now fire correctly, sorry bout that
```
{% endcode %}

### Version 1.2.7 (2023.11.16)

{% code overflow="wrap" %}
```yaml
Stellaris 3.10.0 Compatibility update (Astral Rifts mechanics disabled for now)

New:
- Added new archaeology site to Mustafar
- Added advanced ship types available for Republic via technology / special projects

Changes:
- Adjusted color of Hutt Corvette, Battleship and Dreadnought

Fixes:
- Quantum Catapult is now hidden behind Extended Features
- Fixed issues with the Kaliida & D'Anjon Nebula
- Added missing small slots on the Swiftsure loadout of the Inexupgnable
```
{% endcode %}

### Version 1.2.6e (2023.10.29)

{% code overflow="wrap" %}
```yaml
Fixes:
- Revan and Sith Emperor now have corresponding Jedi and Sith traits

Changes:
- Reactivated Habitat limits: once the initial technology is researched, an empire can support 2 habitats.
  This can be increased through the Ascension Perk (+4) or the advanced habitat technology (+2). No limit for void dwellers.
- Updated Hutt science ship
- Removed hundreds of moons in the galaxy (new game required)
```
{% endcode %}

### Version 1.2.6d (2023.10.17)

{% code overflow="wrap" %}
```yaml
Stellaris 3.9.3 Compatibility update

Fixes:
- Habitat view fix
- Resolution to turn a vassal into a Member World will now set the correct agreement
- Fixed issue causing unique deposits to be removed after finishing the arcology project

Changes:
- Leaders with the Mercy Corps trait can no longer be cloned

New:
- Three new anomalies by Steph/Chronic Ambivalence
```
{% endcode %}

### Version 1.2.6c (2023.10.09)

{% code overflow="wrap" %}
```yaml
Fixes:
- First Contact messages will now always appear after finishing it
- Fixed human hair choices for scientists & governors & pops
- Gave orbitals habitat models

New:
- Keldabe deposit on Mandalore

Changes:
- Disabled vanilla presapients (new game required)
- Disabled Psionics Expertise trait
- New localization for Sundari
```
{% endcode %}

### Version 1.2.6b (2023.10.04)

{% code overflow="wrap" %}
```yaml
Fixes:
- Orbital bombardment icon offset
- Vitiate's traits display issue (backwards compatible, wait till next month)
- Parthovian Cluster deep space hyperlanes destinations corrected
- Removed duplicate tech Arcane Deciphering
- Perma is now part of a Major Trade Route (new game required)
- Fixed Advanced Holographic Entertainment effects tooltip
- Fixed CTD on Republic Accession event (effects delayed by 1 day to prevent CTD)

Changes:
- Vitiate's traits icons and modifiers changed
- Disabled Isotope-5 Extraction Ascension Perk (needs a full rework)

New:
- Added warning event for players playing an Unknown Regions empire on the small map
```
{% endcode %}

### Version 1.2.6a (2023.09.26)

{% code overflow="wrap" %}
```yaml
Stellaris 3.9.2 Compatibility update

Changes:
- Disabled Kaleidoscope event chain
```
{% endcode %}

### Version 1.2.6 (2023.09.23)

{% code overflow="wrap" %}
```yaml
Stellaris 3.9.1 Compatibility update

Mandalorian Rework:
- The Mandalorian Situation stage 5 is now gone.
- The Crusade now starts and ends in stage 4, which has 5 main goals.
- The Crisis Menu has been removed and replaced by Situation Log goals.
- New events for the conquest or subjgation of Althir, Cathar, Roche, Onderon, Taris, Alderaan and Coruscant.
- Some changes to map in Mandalorian Space area.

New:
- Echani Command as a playable empire.
- Althir minor empire.
- New Hutt shipset.
- A few new deposits for certain worlds.

Fixes:
- Missing loc for Survival tree.
- Something is now something else.
- Dralshy'a description display issue.
- MP issue with Republic formation should now be gone for good!
- New icons for Enmity tradition tree.
- No more pre-sapients on planets turned to barren by habitable worlds slider.
- Mandalorian Situation will no longer get stuck if the Warlord fleet gets killed by stupid space whales :p
- Western Reaches custom Empire description no longer states it's not playable on small map.
- Players can now always modify their species after picking the Engineered Evolution AP.

Changes:
- Restored hyperlanes to Dathomir.
- Updated Fantail-class texture.
```
{% endcode %}

### Version 1.2.5i (2023.08.04)

{% code overflow="wrap" %}
```yaml
Fixes:
- Disabled deprecated Defensive Protection warning
- Conquer Nature agenda is now available for lotor adaptability equivalents (Mandalorians and Alderaan)
- The event options for "The Mandalorian Question" event now correctly have different results
- The Sith custom civic now also opens up a Council Position
- Added a clearer description for custom Ascension Perks 'Experimental Gravity projection Methods' and 'Slave Rigging'
- The Republic vassalizing an empire already applying to join will now work properly
- Locked Galactic Wonders AP behind Extended Features

Debug:
- Partial fix for rare case where the Supreme Chancellor isn't a member of the Republic
```
{% endcode %}

### Version 1.2.5h (2023.07.27)

{% code overflow="wrap" %}
```yaml
Features:
- Ahto High Court council room; originally created by Walsh

Fixes:
- Onderon starting ruler fixed (sorry bout that)
- Fixed mandalorian salvaged battleships missing weapons and other slots
```
{% endcode %}

### Version 1.2.5g (2023.07.19)

{% code overflow="wrap" %}
```yaml
Fixes:
- Possible fix for when Jakellians or Mandallians have been destroyed by primitive vanilla events
- Mandalorian elections will no longer trigger on new game start
- Mandalorians and Onderon no longer have access to Adaptability (since they already have a custom one)
- Custom Empires will no longer start with an incorrect planet class
- Several localization fixes
```
{% endcode %}

### Version 1.2.5f (2023.07.04)

{% code overflow="wrap" %}
```yaml
Fixes:
- Removed unneeded hyphens in many events
- Possible fix for Sith event Lost Colony
- Massasi species does no longer get purged by default, and can become soldiers
- Disabled choice of starting solar system for Custom Empires, to prevent invalid Custom Empires
- Coruscant can no longer be diplo-vassalized
- Republic founders comms traded will now correctly advance the Situation
```
{% endcode %}

### Version 1.2.5e (2023.06.24)

{% code overflow="wrap" %}
```yaml
Features:
- Added Councilors for Lotor Civics
- Added Council rooms for several empires 

Fixes:
- Fixed an issue with Kuat fighters
- AI empires are no longer able to leave the Republic (to prevent Member States from leaving)
- Mandalorian Overwhelm tradition now unlocks the Agenda as stated
- Necrophage Origin custom empire no longer starts without pops and buildings
```
{% endcode %}

### Version 1.2.5d (2023.06.16)

{% code overflow="wrap" %}
```yaml
Fixes:
- Possible fix for Coruscant issue in mp, where they are Supreme Chancellor but NOT in Republic...
- Verpine player start was missing deposits that allowed the building of resource districts
- Fixed issue with some ship designs not being created instantly
- Disabled vanilla pre-ftl situation that could cause them to become a hive
- Fix for a lotor digsite where you could select an option even if out of a resource
```
{% endcode %}

### Version 1.2.5c (2023.06.15)

{% code overflow="wrap" %}
```yaml
3.8.4 Compatibility Update

Fixes:
- Some localization and error log fixes
```
{% endcode %}

### Version 1.2.5b (2023.06.10)

{% code overflow="wrap" %}
```yaml
Additions:
- New Interceptor IV & SPV models

Fixes:
- Vitiate's Empowered traits are now councilor traits
- Champions of the Empire tradition should now work correctly
- Jedi event will no longer trigger for wrong countries, only Coruscant
- Fixed case of Sith player getting the crisis fleets in 2375 (should be AI only) if they hadn't finished the special project yet
- Agendas for Lotor custom traditions should now work as intended
```
{% endcode %}

### Version 1.2.5a (2023.06.03)

{% code overflow="wrap" %}
```yaml
Fixes:
- Fixed rare case where Alsakan Conflict's outcomes were wrong
- Fixed not so rare case where other Founders picked the wrong side if it came to war in the Alsakan Conflict
- Sith early game events should now trigger correctly
```
{% endcode %}

### Version 1.2.5 (2023.05.31)

{% code overflow="wrap" %}
```yaml
Compatibility patch for 3.8.x

New Feature: Birth of the Republic situation
- Starts for all Founders on year 1
- Four Stages:
  - Exploration (ends with First Contact with another Founder)
  - Expansion (ends with First Contact with all Founders)
  - Diplomacy (ends with the forming of the Republic)
  - Unification (ends with the Unification event)
- Members Worlds now cede everything except home system and systems one jump from it to Coruscant on Unification
- Players no longer have the option to become Member Worlds
- Vassals of Member Worlds now become vassals of Coruscant
- Unification event now triggers for all Republic members, not just founders, with a 25 and 99% chance to accept respectively
- Greatly reduced Republic Member Worlds research and strategic resources contribution
- Reduced Republic resources subsidy to Member Worlds
- New event chain 'Alsakan Conflict':
 - Triggers during the last stage of the Republic Birth situation
 - Alsakan Monarchy can only join the Republic after the chain concludes
 - Multiple events triggering to all Founders and Alsakan which influence the end result
 - 4 different end results depending on each empire's decisions during the chain
 - 2 of the end reults create a hyperlane between Utrost and Alsakan
 - The more a player takes Alsakan's side, the more likely the chance of getting them as a Member World

Large Map Reworks:
- Included new ancient hyperlanes to the deep core
- Reworked a lot of systems
- Turned about 100 empty systems into randomly generated systems
- Improved loading times via batch hyperlane formation
- Added about 15 Nebulas to the galaxy map
- Added new Nebula Deep Space Hyperlanes accessible via Tech
- Completely reworked the Southern Unknown Regions with some random hyperlanes
- Added various missing Unknown Regions empires (Vagaari, Ssi-Ruuvi...)
- Reworked Unknown Regions entry points
- Various background tweaks regarding maps etc.

Reworked the Economy of:
- Spice: Synthetic Spice Production. The Deposit now allows the new Spice Mining Operation Building (with Jobs)
- Fuel: Synthetic Fuel Production. The Deposit now allows the new Fuel Extraction Pump Building (with Jobs)
- Isotope5: The Deposit now allows the new Isotope-5 Extraction Site Building (with Jobs)
- Medical Supplies: The Deposit now allows the new Medical Supplies Extraction Site Building (with Jobs)
- Synthetic Spice now consumes Minerals
- Synthetic Medical Supplies now consumes Food
- Synthetic Fuel now consumes Minerals
- New localizations for all new Jobs & Buildings
- Reworked Medical Supplies Technologies from 3 -> 8 Technologies (Medical Droids, Bacta Tanks etc.)

Mandalorian Crusade:
- Mid-game year no longer used for AI Crusade, since it now uses the Mandalorian Crusade situation
- The Mandalorian Crusade will no longer start in 2275 even if they haven't completed the Crusade situation
- Mandalorian Crusade start now stops all mandalorian AI offensive wars (no truce)
- Mandalorians can now pick Armageddon Bombardment while on Crusade
- Mandalorian AI will now auto-complete the Industrial Reforms special project after 5 years
- Onderon's chance to submit to Mandalorian Invasion is now based on Crisis Strength (50% at max crisis strength, 10% at default 1)
- Mandalorian Invasion Force on Onderon now scales with Crisis Strength (80% of Onderon's naval cap at 5, 10% less per level below 5, down to 40%)
- Mandalorian AI now gets a fully upgraded StarFortress (tech allowing) with 6 shipyards plus Manufacturer when Crusade starts
- Improved mandalorian Abandoned War Fleet event tooltip
- The Mand'alor is now only the ruler
- No more extra fleet when he spawns (you'll still get a lot of fleets when Crusade starts)
- The Crusade now ends when a Player or a Major Empire (as war leader) wins a war against the Mandalorians

Added new habitat districts for the new types:
- Medical Research Facilities
- Criminal Hub
- Cloud City
- Hydroponic Bays (for food-habitat as a decision to add it)

New Feature:
- Added backwards compatibility for patches, so that no new game is needed in many cases (thx to @24 for the tip)

Additions:
- Added New Playable Empires: Ithorian Herds, High Council of Cerea, Dorin League, Attrasian Commonwealth
- Reworked Planet States into Minor Empires (normal empires that don't expand)
- Added Primitives: Miraluka, Vratix
- New Deficit Situations for Medical Supplies, Fuel, Crew, Heavy Alloys and Spice
- New Republic elections situation: no changes in behavior, just makes it easier to follow
- The Supreme Chancellor's elections now has its own UI, called from a new Tab in the Government UI (and from the new situation)
- The current Supreme Chancellor now has a Unique Leader Trait
- New Deficit Situations for Medical Supplies, Fuel, Crew, Heavy Alloys and Spice
- 6 new sound effects by NickTheNick for early game Republic events
  we realized most of our Lotor custom events don't have a sound effect, so help us identify them please
- Added a unique Hutt City Set
- Added tier 2 and 3 alloy production upgrade techs
- Added Policy: Healthcare - usage of medical supplies on your population
- Added Conqueror Corvette
- Additional clothes for Jedi
- Additional code for Whills leader clothes
- New clothes for various roles
- New animated Ithorian portraits
- New Kel Dor portraits on a different mesh
- Vastly improved Gran portraits
- Additional loading tip quotes
- Added 5 new digsites
- New City sets: Alderaan, Republic, Hutts, Nabood-like
- New Hutt flag
- New situation icons for Massassi and Evochi situations
- New homeworld deposits for each Empire (roughly 30 in total)
- New tiles and icons for the 3.8 traditions Aptitude and Statecraft

Changes:
- All countries now start with 2 shipyards instead of 1 shipyard and 1 trading hub (AI really needs this)
- All Empires now start with 1 Ruler, 1 Scientist (Council and science ship), 1 Admiral (Council and fleet) and 1 Governor (Core Sector)
- Greatly reduced Republic Member Worlds research and strategic resources contribution
- Coruscant no longer gets first contact with the Jedi in early game
- Onderon and Alsakan player will now know why they can't join the Republic at the start
- Disabled most of vanilla comms-spread, so that Sith and Outer Rim contacts happen later
- Completely reworked Jedi and Sith Traditions
- Reworked Jedi and Sith Traits (from 2 --> 8)
- Rebalanced the Jedi Guardian
- Jedi and Sith buildings localizations improved
- Updated Jedi Shrine and Sith Temple buildings
- Reworked some Lotor buildings ai weights
- Vitiate ship event chain now starts a few years later with some fixes
- Updated some Deposits with new art and effects
- Improved Jewel of Yavin Icons
- Improved Decision & Planetary Modifier Icons
- Improved Cloaking Generator + some other Component Icons
- Reworked Cloaking Generators a bit (only lvl 4 can be on dreadnoughts for example)
- Changed Economic Category of Heavy Alloy, Fuel & Spice Jobs
- Fuel and Crew now use a new specific max amount
- Improved many Job Icons
- Rebalanced several economic technologies to be closer in line with vanilla
- Improved Fuel Tech Icons, added on gamestart fuel extraction tech
- Improved Naval Recruitment policy
- Removed Major Trade Routes from start options (no longer used)
- Updated new game AI Empires setting with new Empires:
  it will now spawn the remaining as non-expanding Regular Empires (instead of City States like before)
  15 to 34 Empires, with the new Default being 34
- Disabled Defenders of the Galaxy AP
- Added Communications Relays effects to descriptions
- Rebellions should now be only named after their capital planet or system instead of species
- Exchange Organisation relocated from Ord Vaug to Atzerri
- Striped down the Lotor UI a lot, to return to a more easy to maintain vanilla-ish state
- Reworked some technologies and late game buildings to be more in line with vanilla
- Reduced Concussion Missile Hull Damage modifier back to Vanilla
- Tweaked Sith Terminus cruiser weapons
- Improved Paladin Heavy Cruiser texture
- Vanilla Frigate renamed to Torpedo Boat
- All ship icons updated. From now on, any ship without its proper icon can be considered a bug!
- Some more DLC checks added
- New custom District icons

Fixes:
- All Player Empires now start with 32 pops
- Humans now all have Coruscant as Homeworld (added human capital world habitability bonus to offset this)
- Coruscant is no longer allowed to leave the Republic after the Unification
- Resolution to turn a vassal into a Member World should now correctly annex all their territory 2 or more jumps from capital
- Mandalorian Twin Spears events display fix to stop confusing players (I hope)
- Fixed gray text in ship manufacturers
- Disabled vanilla anomaly that destroys planets
- Fixed verpine economy
- The Voss will no longer have horse faces :p
- Sith event that upgrades Dromund Kaas station now also builds the Sith Manufacturer
- Removed time limit from Sons of Modon's special projects that could make the storyline get stuck
- Improved a handful of clothes for humanoid_05_female portraits
- Humanoid_05 species now have proper Republic ruler uniforms
- Added missing clothes for certain meshes
- Sentient Property AP no longer available for Hives
- Talia Kira now starts a new game assigned as Core Sector governor
- Fixed verpine economy
- Fixed gray text in ship manufacturers
- Disabled vanilla anomaly that destroys planets
- Vainglorious missing hangar fixed
- Chiss no longer start with unique recruitment (will correctly come from a Tradition)
- Sith area hyperlane fixes
- New capital colony types now also give amenities
- Members Worlds now share comms with Coruscant on Unification event
- Technologies reducing Starbase Crew Upkeep are now working correctly (with a workaround until PDX fixes starbases_xxxx_upkeep_mult)
- Shattered Ring custom empires no longer start without a capital_scope set, causing all sorts of problems
- Dozens of minor fixes
- No more repeating Dark Side trait events
- Mandalorian warriors are now generic (no species)
- Sith and Jedi buildings are now demolished if planet is conquered by someone not supposed to use them
```
{% endcode %}

### Version 1.2.2d (2023.04.10)

{% code overflow="wrap" %}
```yaml
Mandalorian Situation:
- Mandalorian Situation phase 1 is now fully event driven (could make later phases start too early).
- Fix for Phase 3 issue if Jakelians no longer exist (could make phase 3 get stuck).
- Reworked Phase 4 to progress to Phase 5 once all objectives are met.
- No more first contact event for when Cathar opens up.
- Reduced Mandalorian invasion force on Onderon by 20%.
- Several minor tweaks.

NOTE: Phase 5 does NOT progress and is reserved for future content!
```
{% endcode %}

### Version 1.2.2c (2023.04.06)

{% code overflow="wrap" %}
```yaml
Mandalorian Situation:
- Fix for event that keeps repeating under some circumstances
```
{% endcode %}

### Version 1.2.2b (2023.04.06)

{% code overflow="wrap" %}
```yaml
Mandalorian Situation:
- Phase 3 bug fixes.
- Fully reworked Phase 3, should now be clearer in some parts.
- All Phase 3 situation related events should now give a small amount of situation progress.
- New mini event chain to help players track 1 of the objectives.
- Improved Mandallia and Jakelia integration.
- Many localizations for the whole Mandalorian Situation reworked by Steph
```
{% endcode %}

### Version 1.2.2a (2023.04.02)

{% code overflow="wrap" %}
```yaml
- 3.7.4 compatibility update and Madalorian Situation hotfix

Mandalorian Situation:
- Fixed a few bugs that could prevent the situation from progressing
- All situation related events should now give a small amount of situation progress (Phases 1 and 2 only for now)
- Phase 1 now progresses slightly slower, making it end in 2217/8, unless you delay the events
- Made Phase 2 mask hidden events visible on the option, so that players know how long to wait (like the start event does)
```
{% endcode %}

### Version 1.2.2 (2023.03.25)

{% code overflow="wrap" %}
```yaml
Changes:
- 3.7.2 and 3.7.3 Compatibility Patch
- Increased tech choosing option from 3 to 4
- Improved the spread of vanilla sr resources (volatile motes, exotic gases, rare crystals)
- Slightly buffed ion cannons & plasma spheres
- Herglic Trading Empire can now join the Republic
- Reworked the Mandalorian Culture Tradition Tree
- Reworked the Mandalorian Crusade Tradition Tree
- Reworked the Coruscantii Pride Tradition Tree
- Reworked some early game Mandalorian Events
- Reworked the Ending of the Mandalorian Crusade (shatters now and spawns two small Mando Empires besides the core sector)
- Improved various ships with glowing hangars
- Nerfed Criminal Syndicates a bit
- Tweaked some ship roles & loadouts
- Consistently renamed all loadouts
- Resized all ships to be roughly 20% smaller but added a new zoom step
- Custom Empires can now pick lotor shipsets ingame
- Rewrote the description of all Ships
- Partially reworked the Tionese Tradition, 
  they now have the ability to declare planets as Tionese Throne Worlds
- Partially reworked the Arkanian Meriotocracy Tradition
  they now have the ability to establish an Adascorp Lab Complex on Arkania
- Partially reworked the Chiss Ascendancy Tradition
  they now have the ability to choose the unique Chiss Recruitment Policy
- Detox AP and the toxic terraforming candidate event are now locked behind Extended features
- Added more glowing hangar shields to various ships
- Added a new Shield-Type, Ray Shields offer increased shield hardening at the price of roughly 20% less regen & strength
- Added individual research amounts to topbar (for screen resolution width of 1600 or higher) - inspired by *UI Overhaul Dynamic - Extended Topbar*
- Reduced thickness of hyperlanes with hyper relay
- Rebalanced alot of Isotope5 Components to be easier to use
- Space City can now get Research Districts
- Republic Members Worlds can now nominate and be elected for Supreme Chancellor
- Added music to Mandalorian Crisis start event
- Added music to Republic Unification event
- Can no longer declare Republic members (or applying) as crisis
- Crisis are now called Galactic Emergencies
- Added cloaking generators (locked behind AP)
- All 12 human empires now share the same species
- Republic Member Worlds renamed to "<Capital System> Sector" on Accession (Alderaan Sector, etc)
- Reworked Primitive System, all primitives will now always spawn 
- Improved Sith Start
- Autoexploration is now available from the Start
- Rebalanced Republic Member World 10% -> 5% Basic Subsidies
- Empire capitals now have extra amenities
- Reworked Mandalorian Events and Crusade into a long Situation

Fixes:
- Orbital Ring shipyards not counting as shipyard
- Fixed minor sith tradition issues & wrong localizations
- Sith now have Arid preference instead of Tropical (for player)
- Dozens of other smallers tweaks & fixes by Harain
- Titanic lifeforms will no longer reward Zillo Scale relic
- Modern Fuel Processes will no longer grant 2 normal and 2 drone jobs regardless of empire type
```
{% endcode %}

### Version 1.2.1 (2022.12.31)

{% code overflow="wrap" %}
```yaml
Additions:
- added three new starbase modules, Flak Batteries, Medium Gun Batteries, Large Gun Batteries - unlocked by starbase technologies
- remove megastructure button addet to fuel depots
- added 3 unique deposits to coruscant post ecumenopolis, to prevent running out of food and energy
- Two new Starbase types with five stages each!

Changes:
- tweaked Ruler of Clans Trait for Mandalore not doing anything
- added lotor weapon buffs to weapon buff edicts, these will be reworked eventually but are usable for now
- Improved the Large map, to cut of the Sith as well
- Lotor Empires will now always have Starwars Mining & Research Station models
- Changed Fuel Depots effect to prevent cheeseable options

Fixes: 
- Minor loc adjustments
- Vaiken can no longer strike out on his own and become a mercenary enclave
- Disabled vanilla cultist and lost labyrinth events
- fixed republic member worlds when playing as coruscant and saying no
- fixed crew training center showing up on orbital rings
- fixed vassals joining the Galactic Republic, even if their Overlord isnt part of it.
- fixed Commrelay Update being spamable
- fixed a bug were Taris could get Republic Support for attacking the Mandos pre crusade
- fixed Laminanium Auxiliary Components
- fixed Transport ship bug
```
{% endcode %}

### Version 1.2.0 (2022.12.17)

{% code overflow="wrap" %}
```yaml
New Systems:
- New Ship System, research the required technology and pick the ship you want from your options!
- New Ship Classes, Heavy Cruiser and Battlecruiser for balance all other Ship Naval Capacity Numbers have been adjusted
- Republic Consolidation, the 4 founding members will become a special vassal type (republic member world) with coruscant as its overlord.
- Other Subjects of Coruscant can become republic member worlds via a special resolution

Additions:
- Unique uniforms for humans in the Hutt empire.
- Added the Herglic Trade Empire as a new Empire in the Southern Core & the Telosian Council in the North East
- Clothing system added for species on other meshes
- Various new Ships to fill the other roles
- New menu music and a few additional tracks.
- Megastructure Delete Button
- Republic Mercy Corps

Theocracy of Onderon:
- Adjusted Start, new Leader and more
- Unique Story events about Onderons History
- New clothes for Onderonian leaders
- a completely new Onderon Shipset

Changes and Improvements:
- Completely changed all weapon component names to actual lore versions
- Adjusted Weapons & AI Roles to 3.6 and the new Mechanics
- New Mandalorian Flag Pre Crusade & New Empire Colours
- New Mandalorian Clothing System, once the Crusade starts the Mandalorians will start to wear the iconic armor
- New Mandalorian Shipset (New Dawn)
- New Tradition Icons for Ascension paths
- Updated various Traditions
- New and Rebalanced Solar Systems
- Improved portraits for various species
- Improved almost all Traditions to follow the New Scheme of Equivalents, which also results in new Additions
- Improved various Origins like for example Kuat, which now starts with an Orbital Ring
- literally hundreds of smaller tweaks, improvements and more
- multiple localisation improvements
```
{% endcode %}

### Version 1.1.9 (2022.10.03)

{% code overflow="wrap" %}
```yaml
Changes:
- new focused lore map 1200 Systems
- changed some Empire colours to prevent the same ones next to each other (for example Alderaan is now green like their old secondary colour)

Fixes:
- fixed overtuned origin not having a starting text
- fixed karst planet not being teraformable
- fixed overtuned origin not getting gene tailoring + mechanoid origin not getting powered exoskeleton and droid tech
- fixed Juggernaut tech showing non existand starbase modules
- fixed Mega Engineering Outliner
- fixed Hyperspace Beacon Text length
- fixed dynamic AI Modifiers tooltip being hard to trigger
- fixed missing game setup setting to mp ui
- fixed starwars droid portraits not being used
- fixed chiss namelist having a empty space
```
{% endcode %}

### Version 1.1.8 (2022.09.24)

{% code overflow="wrap" %}
```yaml
- updated to 3.5.1
- disabled Toxic God Origin (thematic reasons)

Changes:
- certain balance tweaks
- colony ships cost no influence anymore (ai related)
- all empires start the game with 1000 crew instead of 200.
- Vanilla Planetary Modifiers can now be randomly spread through the galaxy (large mineral deposits, heavy thunderstorms etc.)
```
{% endcode %}

### Version 1.1.7 (2022.08.28)

{% code overflow="wrap" %}
```yaml
Changes:
- (general) reduced the primitive spawning chance from 50% to 25%
- (sith) changed how the solar system for Vitiate's second quest is picked, it shouldn't be inside Tionese border 
- changed starting pops from 28 -> 32 like in vanilla (improves amenity issues)
- Improved Capital Designation with additional 10 amenities
- Reworked Revolts to never target the home system of any Empire (or planets inside it)

Fixes:
- (locs) fixed Alderaan home planet name in empire selection screen
- (locs) fixed typos reported by translators
- (events) Tiyanki first contact picture replaced with Purrgil
- (sith) fixed POI not showing for the second special project of Tending the Garden quest
- fixed science ships in the starting menu being in a wrong position
```
{% endcode %}

### Version 1.1.6 (2022.08.03)

{% code overflow="wrap" %}
```yaml
Additions:
- new Shield Capacitor Icons
- new Ion Engine Icons + naming scheme
- New Holding Building Icons 1/4
- New Main Menu Option - Chance based Primitive Spawn, if turned off all Primitives will spawn
- Added Criminal Corvette

Improvements:
- Fixed Mandalorian AI and tweaked multiple values related to them
- Added redundencies to make Mandalore the Ultimate not dismissable as a leader + no Mercenary Enclave
- made Sith and Mando more likely to get Galactic Force Project
- Adjusted Hull & Armor Regen comps because of Vanilla Engine Tweaks in 3.4.4
- Buffed Ion Weapons
- Added a special Planet Modifier to Varl (Hutt) to make the start with multiple slave races a bit easier
- Rebalanced Fighter and Bomber Numbers, decreased Fighter from 8 -> 6 and Bomber from 6 -> 4
- Fighters & Bombers from Tier II and Tier III now cost additional rare crystals/exotic gases
- Added rare crystal costs to tier 4-6 of point defense
- Everyone starts with 3 (2+1) base envoys
- Mandalores Fleet can no longer be changed but still reinforced to prevent an ai bug
- Mandalorians and Sith can now pick the Supremacy Policy without the Tradition
- Updated Various Planet Models
- Buffed Armor gain from Heavy Armor Components, but they reduce the evasion now
- Mandalorian Bulkheads reduce the ship speed now
- rebalanced Arkanian Tradition
- rebalanced shield types - Thanks Gamgy!
- added other species for Zygerrian's Buy Slaves option
- minor stuff

Fixes:
- fixed Slugthrower not auto upgrading past tier 4
- fixed not scaling xl weapon cost
- fixed Pure Machine Empires having no Armies
- fixed several uniforms
- fixed requires_no_communication_relay_nearby rule looking at other empires relays as well
- fixed robot --> droid
- fixed Enclaves/Planet States not contacting you after 2/3/4 years
- fixed wrong values on tier 6 turbolaser + ion cannon
- fixed titan missing its second xl weapon
```
{% endcode %}

### Version 1.1.5 (2022.06.21)

{% code overflow="wrap" %}
```yaml
Added:
- (general) 3.4.4 compatiblity
- (general) a couple of new animal deposits
- (general) added Axamine Dreadnought Leviathan
- (arkanian) new Arkanian tradition tree "Meritocracy"
- (mandalorian) new Mandalorian Light Vehicle & Heavy Vehicle armies
- (mandalorian) new Mandalorian Crusade Remnant flag
- (hutt) new Hutt species trait
- (hutt) new custom Hutt armies
- (hutt) added Evocii situation replacing the Unsatisfied Customers event
  * two possible outcomes
  * Evocii raids will aim destabilize your planet and reduce the situation's progress 
  * your approach and choices influence how often raids happen

Improvements:
- (ai) tweaked AI to use less corvettes after battleship are researched
- (performance) changed primitive spawning algorithm to prevent all primitives from spawning every game, this should increase performance after mid game
- (general) improved animal deposits
- (mandalorian) Mandalorian Crusade event fleets can now be upgraded and reinforced (the 0% chance of combat disengage is still there)
- (hutt) reworked the Hutt tradition "Glorious Hutt Empire"
- (sith) reworked Sith Agency tradition

Fixes:
- (general) mercenaries now rebuild their ships + use the correct graphical culture
- (general) fixed force sensitive/jedi/sith droids
- (general) fixed Juggernaut military view using vanilla colors
- (general) fixed player empire's ruler being a xeno
- (general) fixed Weak Signal anomaly spawning on objects without a surface
- and other fixes
```
{% endcode %}

### Version 1.1.2 (2022.05.30)

{% code overflow="wrap" %}
```yaml
Improvements:
- Added unique Trait to Admiral Vaikan
- changed Subjugation power value (should prevent everyone becoming a vassal)
- resized the rest of the existing ship sets (everything should have its cannonical size now)
- updated various planet backgrounds
- tweaked a lot of AI Things to result in more Ship Building and a better Economy
- tweaked even more AI Budget related things to make the AI build more Armies & Ships
- changed Mercenary and Unknown Contact Naming Scheme to be more Star Wars like
- added new Mercenary Room

Fixes: 
- fixed Sith Admiral Vaikans Portrait
- fixed Volatile Mining Building having the wrong icon
```
{% endcode %}

### Version 1.1.1 (2022.05.25)

{% code overflow="wrap" %}
```yaml
Improvements:
- added some lotor species to unknown species
- replaced the custodian icon with a custom icon
- Sith Species are now extremly Adaptive (on new game)
- Overlord Vassals now use Lotor Resources in their Agreements
- Mandalorian Crusade AI now gets same ethics as at start

Fixes: 
- added missing orange overlay to several buttons and tiles
- fixed planetary army ui cutting of resources
- fixed sith colony picking desevro or other early tion colonies
- fixed some music volume tracks being way to loud
- fixed Destroyer tech unlock not saying Frigate
- fixed some clothes not fitting 100%
- fixed influence icon not leading to claims
- fixed floating turret issue on Outer Rim Starbase
```
{% endcode %}

### Version 1.1.0 (2022.05.20)

{% code overflow="wrap" %}
```yaml
Rework:
- Reworked primitive spawning mechanic, now only three primitives will awaken at a given date after the midgame pulse
- Reworked medical supplies, they can now be produced and have more uses
- Reworked Trade Routes with the new Hyper Relays
- Adjusted Human Portraits to new Vanilla Human Portraits with fitting clothes
- Updated Chiss, Arkanian and Zeltron Portraits
- Reworked some Lotor Traditions (Multiple Things + New Stuff)
- Everyone can now Start with Basic Unity Buildings again (Temple, etc.)
- removed Oridium as a resource, components still exist but use alloys/heavy alloys now
- Reworked the Complete Techtree, some techs got changed, some deleted, some added.
- Reworked the entire Weapon System, new element types (Laser, Radiation, Explosion etc.) and more
- Reworked Torpedo & Missile System, you can now choose which one you want with some limitations

Added:
- Texticons for weapon and missile components
- Unique tradition trees for Czerka and the Muun Banking Clan and Tion
- Added icons for multiple traditions
- New Weequay portraits from DeathByRats
- Added Evocii, Voss, and Umbaran primitives and portraits
- Added seismic torpedo effect and sound
- Some new Clothes and Hair
- Added shko-yagu for trandoshans as a cloning replacement mechanic
- Added Seismic Charges and Atomic Missiles

Changes:
- Primitive and Planet States now spawn with 4 and 10 pops
- "Warrior Culture" tradition is more cohesive and added two new perks (Mando'a and Foundlings)
- Republic member now leaves the Republic if vassalized by outsider

New Crisis System:
- New resolution category 'Intervention'
- New resolution sub-categories 'Intervention Policy' and 'Galactic Emergencies'
- Interventions will become available 10 years after the Republic's formation when a member is under attack by an outsider
  - The first time it happens, the resolution 'Enable Defensive Protection' becomes available and must be passed first
  - After that, a targeted resolution 'Defensive Protection: <target>' becomes available to defend a Republic member under attack by an outsider
  - This resolution targets the Republic defender, and once it passes, every Republic member will get the option to join him on all defensive wars
    (only Federation leaders will get this option (rest will auto join), because of a vanilla limitation with joining wars by script)
- Galactic Emergencies will become available when a LotOR crisis starts (Mandalorians and Sith only atm)
  - As soon as the Crisis starts, you can pass the resolution 'Enable Galactic Emergencies' (easy to pass)
  - After that, a targeted resolution 'Declare Galactic Emergency: <target>' becomes available, but at first will be impossible to propose/pass
  - All offensive actions taken by a Crisis empire will increase its threat level
  - Threat increases by taking systems (lowest), declaring war, occupying planets, destroying an empire (highest)
  - AI wilingness to propose the resolution is directly proportional to threat level (with an extra value coming from years passed since crisis start)
  - If the Crisis empire attacks a Republic member, the AI wilingness to propose the resolution will rise drastically
  - Once it passes, and IF a Republic member is under attack by the Crisis, ALL remaining Republic members will join the defensive war
    If not, all remaining Republic members will join the defensive war once the Crisis empire attacks

Mandalorian Crusade ending:
- The Crusade will end as before when the Mand'alor gets killed after his second defeat
- The empire that kills him will receive the Mask of the Mand'alor Relic
- All wars involving the Crusade will be ended
- Half the fleets will be randomly destroyed
- A new country called Mandalorian Crusade Remnant will be created
  - All but a few systems around Mandalore will be transfered to it
  - All but a couple of the remaining fleets and half the admirals will be transfered to it
  - It will be hated by all regular empires and a new total war CB will be available against it
- New flavor events for the split of the Mandalorians (Mando, Sith and generic)

Fixes:
- Spectrum lasers are now pointy
- AI versions of empires have their unique government and ruler titles
- Neimoidians no longer spawn on Evocar during event chain
- The AI will no longer pick the Individual Cloning Ascension Perk
- Togrutan rulers will no longer have blank names
- Hives can now use Selective Terraforming
- Pyke PS will no longer sell Hutt slaves
- Lots of localization fixes
```
{% endcode %}

### Version 1.0.8 (2022.05.04)

{% code overflow="wrap" %}
```yaml
Changes:
- Added war declaration option to Planet States
- Slightly reduced crisis strength on small map
- Great Crusade wargoal no longer allows for Status Quo

Fixes:
- Mand'alor flavor events will now only trigger for players with comms to Mandalore
- Crisis Menu no longer disabled by using requests
- Crisis Menu request 'New vode' now correctly converts other pops to Mandalorians
- Crisis Menu request 'New akaanse' now creates armies of mandalorian species
- Mandalorian Crusade chain counter no longer increases past 6
- Set missing species event targets for players
- Driven Assimilator and Rogue Servitor now spawn with a second (Star Wars) species
- Released vassals will no longer have empire unique civics
- Jedi tradition will no longer be given automatically to players in the Republic (talk to the Jedi Order instead)
- Sith emperor traits will no longer buff everyone instead of just him
```
{% endcode %}

### Version 1.0.7 (2022.04.28)

{% code overflow="wrap" %}
```yaml
Changes:
- The Mand'alor's fleets will now scale with game difficulty and Crisis Strength game setup value
- Increased AI crisis strength slightly
- AI is now less likely to vote for an empire that is in breach of any galactic laws in the supreme chancellor election
- AI rebellion now only occurs for custom empires
- Added info to diplomacy screen for members of the Republic
- Imperial Prerogative AP now gives -25% to Empire Size from Colonies and Districts
- Disabled several vanilla events and anomalies that nuke primitives

Fixes:
- Vanilla crisis will no longer trigger
- Removed the luring the crisis espionage option
- The Jedi Order will no longer talk to the Sith
- Adjusted values for tradition perks that used espionage_operation_speed_mult
- Added missing enclave system flags (for Enclaves Incorporated)
- Added missing generic starting descriptions to 5 empires
- Fixed Xeno ruler for players (for good this time)
- Slave Armies are now available for players
- Fixed progression for Founding and Lesser Houses trees for Alderaan

Mandalorian Crisis Menu changes:
- The Crisis Menu Objectives buttons now gives you a Point of Interest (POI) for each objective
- Each objective will have pre-requisites to become available (tooltip will state them)
- The 'Take Cathar' objective will also create a hyperlane to Cathar IF you own an adjacent system (note: big lag spike on large map)
- If no system adjacent to Cathar is owned once you select it and get the POI, the hyperlane will be created once you take any of those systems (see note ablove)
- Cathar must be taken before the Taris objective becomes available
- New Objective to take Onderon
- Taris must be taken before the Onderon objective becomes available
- Destroy the Drengir objective is now optional
- New Events for entering Cathar system, taking Cathar, taking Taris, taking Onderon and defeating the Drengir
- New flavor events for players if the Mandalorian Crusade AI takes those objectives
- Rewards added for each objective
```
{% endcode %}

### Version 1.0.6 (2022.04.20)

Requires a new game start to reflect all changes.

{% code overflow="wrap" %}
```yaml
Savegames:
Continuing a 1.0.5 game with this version will remove some traits from leaders. This is caused by these changes.
- Changed the immortality trait a bit, everyone gets a generic now
- Changed a few more traits and their coding names

Tiny Outliner:
From this version onwards you need to have tiny outliner BELOW lotor in the playset because we improved the outliner without the mod.

Rework:
- completely reworked how empire species are created to fix all the double species problems for new games

Changes:
- Reworked our combat speed aura, the culprit of the crashes with too many fleets in 1 system
- Removed cap of number of Crisis fleets (beware of Crisis Strength 5)
- Becoming the Crisis trait is now locked behind Extended Features
- Reduced starbase strength a bit
- Changed the placement rules for Communication Relays. They now need to be at least 2 jumps away from each other
- Communication Relays now give unity and amenities buffs to system planets
- Mandalorian Crisis starting event will now be much more noticeable and limited to players that have met them
- Players that never met Mandalore will now get a warning event when they meet them if the crisis has already started
- Pops converted by Mandalorian Training Camp will now also convert to the highest approved faction
- Mandalorian Crisis Menu options now have a Back option
- Mandalorian Crusade and Sith Resurgence now have access to Armageddon bombing stance
- Empires are now more likely to vote for others in the Supreme Chancellor election if they are in the same Federation or have a Defensive Pact
- The Palace of the Jedi now allows the recruitment of 6 extra Jedi armies
- Changes to the aquatic preference values
- Improved a couple of trait icons
- Changed backgrounds for trade, military and hegemony federations
- Renamed the Jedi Temple on Coruscant to Palace of the Jedi
- Renamed Juvenile Space Amoeba to Juvenile Neebray

Fixes:
- Fixed Ascension perk button hitbox
- Fixed Killiks not being able to assimilate species if they took the Evolutionary Mastery AP
- Fixed empires showing vanilla start screen messages
- Fixed Coruscant Ecumenopolis event removing ice crypts and imperial palace deposits
- Losing a planet containing a Jedi Enclave/Temple or a Sith Shrine/Academy will now reduce the number of available armies to recruit
- Jedi and Sith temple upgrades now only increase army cap by 2 (for a total of 5)
- Player Mando Crisis will now again trigger Era change regardless of game date (enabling player Crisis button)
- Glorious Expansion tradition now correctly reduces Starbase influence cost
- Mandalorian Crisis menu cooldown tooltip now displays correctly
- Mandalorian Crisis menu option "New vode" will no longer turn droids into Mandalorians
- Fixed Mandalorian Crisis Menu opening several instances
- The Great Crusade special project now ends
- Fixed Mando government not counting as authoritarian
- Some Mando Crisis Menu localization
- Fixed two technologies giving trade value in every system
- Fixed Bypasses not usable when at war
- Fixed FTL inhibitor buildings not showing
- Beneath the Ice Anomaly now gives Medical Supplies
- Fixed Jedi and Sith getting force-sensitive traits
- Fixed species Jedi armies
- Sith tradition "Sphere of Influence" now correctly grants 1 influence
- Influence bonuses from civics and traits are now correctly applied
- Cloning a leader now actually costs energy and medical supplies
- Isotope 5 Shield Capacitor can now also be installed on Juggernauts
- Fixed Isotope 5 AP special project issue
- Universal Prosperity Mandate resolution will no longer reduce special resources like crew so drastically
- Fixed two issues with Crew Extraction Methods
- Fixed nonscaling component power
- Fixed certain Combat Computers not upgrading past tier 3
```
{% endcode %}

### Version 1.0.5 (2022.04.15)

{% code overflow="wrap" %}
```yaml
Fixes:
- Possible fix for duplicate pops on new game
- Empire destroyed when nominated for Republic elections will no longer have strange results
- Republic election messages are no longer sent to Players outside the Republic
- Female leaders finally have proper titles (working on it nowish)
```
{% endcode %}

### Version 1.0.4 (2022.04.14)

{% code overflow="wrap" %}
```yaml
Content:
- added a few black holes to the small map

Changes:
- Disabled declare crisis Resolutions
- Added an extra warning for Custom Empires required traits
- Added mineral income as planet state option for Verpine
- Minor changes to the Reclaim Mand'alore's Mask war goal and follow-up events
- Improved some deposits
- Adjusted various society starting technologies to make important ones show up easier

Mandalorian Crisis:
- Mid-Game Date no longer triggers Crusade for Player, only AI. The player must do the storyline
- Mandalorian fleets spawned by the Crisis will now be twice as strong but reduced in number by half
- Capped number of armies and fleets spawned to avoid a crash when too many ships are in a system
- Fleets will now spawn randomly in any owned Starbase

Mandalorian Crisis Menu:
- Cathar now properly gets hyperlanes through the relevant event; now also takes into account galaxy map size
- Conquer Cathar point of interest is now properly removed after Cathar is conquered
- Requesting fleets through the Mando Crisis Menu now requires Cathar to be conquered
- The Conquer Cathar goal in the Mando Crisis Menu now disappears after Cathar is conquered
- The localization for the Cathar goal now states that taking Cathar is required in order to request fleets

Fixes:
- Fixed Trade Routes allowed to be built in other empires
- Supreme Chancellor's empire destruction will no longer break the Republic elections process
- Servitude droid pops will no longer become unemployed when new pops grow
- Megastructures not showing up in technologies with extended features
- Fixed Killik Joiners Culture civic not allowing assimilation without AP
- Fixed Killiks having two species at game start
- Fixed Hutts enslaving Hutts on game start even though they have over 35% slaves
- The Sith Emperor will no longer be considered a Xeno scum (for real I hope)
- Ahto Planet State now has Selkaths instead of Humans
- Weird progression in the Chiss tradition tree is resolved
- Imperial Agents tradition finisher now functions properly
- Possible fix for droid rebellions without government and civics
- Primitives now have full ethic values
- Resource Silo will now correctly display minerals instead of duplicate energy
- Renamed Imperial Palace to Presidential Palace
- Fixed missing "Back" options for some of the Jedi's submenus
- Fixed "Tell us about the Order" triggering both an event and a response-text
- Proper female ruler titles for Czerka
- Lots of typos and capitalization fixes
```
{% endcode %}

### Version 1.0.3 (2022.04.11)

{% code overflow="wrap" %}
```yaml
Content:
- New Jedi Guardian trait for the Jedi provided by the Jedi Order
- Added four systems to fix a large hole without any planets in the slice's inner rim
- Added wip Klatoonian portraits instead of using a vanilla humanoid one
- Czerka civic effect; 10% trade value
- Added ruler titles for czerka and exchange
- Renamed Space Amoebas to Neebrays

Change:
- Made new improved Jedi trait icons
- Admin offices are now filtered by unity instead of government
- Alien Zoo now gives a small amount of society research

Fixes:
- Set a maximum to Mandalorian crisis fleets to avoid crashes with max difficulty settings
- The Sith Emperor will no longer be considered a Xeno scum!
- Player controlled Corporate Empires can no longer join the Republic before resolution passes
- Wrecked Sith Shipyard event now grants all buildings it's supposed to
- Buying slaves from Planet States will no longer reset slavery type
- Starbase buildings that need a shipyard now also work with special shipyards
- Sith Architecture technology can now only be obtained via Tradition (Rebuilding the Shipyards)
- Empire applying to the Republic now gets comms with all republic members
- Crisis warning event no longer triggers for players that have no contact with any Republic member
- Prevented Oracle governor from being cloned
- Renamed the second Troithe planet to Verzan
- The Flesh is Weak AP now shows the correct requirement
- Droids can no longer work as Jedi Monk or Sith Acolyte
- Disabled vanilla event that spawned a wormhole
- Fixed habitability slider not working in multiplayer games
- Fixed AI declaring itself a crisis
- Fixed first colony event showing the wrong desc for Planetary Diversity planets
- Fixed main species not always having full citizenship for custom empires
- Fixed hyperlane charge sound repeating itself
- Fixed newly build habitats not getting the trade route modifier
- Fixed starbase crew module costing consumer goods for gestalt empires
- Fixed Symbol of Power not spawning admiral leader
- Fixed human clothes not appearing correctly
- Fixed Korriban getting turned into barren from habitability event
- Lots of typos and localization fixes
```
{% endcode %}

### Version 1.0.2 (2022.04.09)

{% code overflow="wrap" %}
```yaml
Changes:
- Improved fighter and bomber loadout icons
- Christophsis is now a crystal world
- Crisis strength now doesn't affect the player when turning into the crisis
- Improved fleet formations

Fixes:
- AI will no longer pick invalid resolution causing a crash
- Scaled crisis strength down if greater than 1 to avoid a crash
- Fixed habitability slider not working
- Fixed Houses of Alderaan event issue on small map
- Fixed "Symbol of" event-chain not firing the Imperial Citadel stage
- Fixed a graphical issue with generic starbases
- Fixed the Arch-Being anomaly outcome
- Fixed sith being able to research light beam weapon
- Fixed droids being Jedis
- Fixed a few leader titles
- Fixed battleship and juggernaut sections icons
- Fixed the hyperlane fix from the last hotfix
- Fixed a lot of typos and other localization issues

Miscellaneous:
- Added remote_file_id to the descriptor.mod file
```
{% endcode %}

### Version 1.0.1 (2022.04.08)

{% code overflow="wrap" %}
```yaml
Changes:
- AI attitude upgrades, fixed Mandalorians being a bit too passive

Fixes:
- AI expanding very slow on lower difficulties
- Ship textures not showing correctly in the Mandalorian shipset
- Duplicate Emerging Power origin
- Adjusted spiritualist heritage tech weight
- Fixed a bunch of techs being declared as rare even though they aren't
- Fixed an issue with the vanilla Space Dragon origin
- Fixed Korriban system on small map
- Fixed endgame hyperlane effect for small map
- Fixed hives not getting medicines
- Fixed AI not building enough colony ships
- Improved various technology weights
- Improved tradition perks of the Mando tradition "Mandalorian Culture"
- AI is now more likely to build military academies when low on crew
- Fixed two random planet states not spawning on the small map
- Possible fix for ctd with invalid resolution
- A lot of typos, localisation and tooltip fixes
```
{% endcode %}
