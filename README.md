---
description: >-
  A Star Wars total conversion Stellaris mod based on the Old Republic era with
  vanilla-like gameplay.
cover: .gitbook/assets/lotor wiki banner.png
coverY: 0
layout:
  cover:
    visible: true
    size: full
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Introduction

{% hint style="warning" %}
**Versions**

These docs are up-to-date for LotOR `v1.3.0`

The mod is compatible with Stellaris `v3.10.x`
{% endhint %}

{% hint style="info" %}
**Disclaimer**

_Star Wars and all of its content belong solely to the Walt Disney Company._

_This mod is not affiliated with them in any way._

_This is entirely a community-driven, non-profit project._
{% endhint %}

## About

**Legacy of the Old Republic** (LotOR) is a total conversion mod for [Stellaris](https://store.steampowered.com/app/281990/Stellaris/) set in the Old Republic era of the Star Wars universe. The mod is developed by the Renegades Modding Group (RMG) and is available on both Steam and Paradox Mods.

The mod offers the players the ability to select and play from over 37 empires in the Star Wars universe. In addition to those, players can also create their own custom empire and spawn in predefined locations around the galaxy.

Embark on a captivating journey within LotOR, taking the helm of the formidable Mandalorian Clans, forging a new path through a compelling Crusade of your own design, or resurrecting the grandeur of the Sith Empire.

Alternatively, opt to establish the Galactic Republic itself, steadfastly shielding the galaxy from the dual threats of the Mandalorians and the Sith, ushering in an era of unparalleled order and stability across the cosmos.

Immerse yourself in novel events, meticulously crafted empire-specific stories from Canon and Legends, and plenty of new event pictures, tech icons, building icons that seamlessly expand the Star Wars universe.

## Links

<table data-view="cards"><thead><tr><th align="center"></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-card-cover data-type="files"></th></tr></thead><tbody><tr><td align="center">Steam Workshop</td><td><a href="steam://openurl=https/steamcommunity.com/sharedfiles/filedetails/?id=2791119024">steam://openurl=https/steamcommunity.com/sharedfiles/filedetails/?id=2791119024</a></td><td><a href=".gitbook/assets/lotor card steam.png">lotor card steam.png</a></td></tr><tr><td align="center">Paradox Mods</td><td><a href="https://mods.paradoxplaza.com/mods/41684/Any">https://mods.paradoxplaza.com/mods/41684/Any</a></td><td><a href=".gitbook/assets/lotor card pdxmods.png">lotor card pdxmods.png</a></td></tr><tr><td align="center">Steam Workshop (Beta)</td><td><a href="steam://openurl=https/steamcommunity.com/sharedfiles/filedetails/?id=2671963433">steam://openurl=https/steamcommunity.com/sharedfiles/filedetails/?id=2671963433</a></td><td><a href=".gitbook/assets/lotor card steam beta.png">lotor card steam beta.png</a></td></tr><tr><td align="center">Join our Discord server</td><td><a href="https://discord.gg/4xfQ78sPpm">https://discord.gg/4xfQ78sPpm</a></td><td><a href=".gitbook/assets/discord cover.png">discord cover.png</a></td></tr><tr><td align="center">RMG mods collection</td><td><a href="steam://openurl=https/steamcommunity.com/sharedfiles/filedetails/?id=2469745470">steam://openurl=https/steamcommunity.com/sharedfiles/filedetails/?id=2469745470</a></td><td><a href=".gitbook/assets/rmg mods card.png">rmg mods card.png</a></td></tr><tr><td align="center">Give a tip to your modder</td><td><a href="https://opencollective.com/renegades-modding-group/contribute">https://opencollective.com/renegades-modding-group/contribute</a></td><td><a href=".gitbook/assets/donate to rmg card.png">donate to rmg card.png</a></td></tr></tbody></table>

## Major playable empires

<table data-view="cards"><thead><tr><th align="center"></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td align="center">Coruscantii Union</td><td><a href=".gitbook/assets/important_empire--coruscant.png">important_empire--coruscant.png</a></td><td><a href="empires/core-region/coruscantii-union.md">coruscantii-union.md</a></td></tr><tr><td align="center">Mandalorian Space</td><td><a href=".gitbook/assets/important_empire--mandalorian.png">important_empire--mandalorian.png</a></td><td><a href="empires/hydian-borderlands/mandalorian-space.md">mandalorian-space.md</a></td></tr><tr><td align="center">Theocracy of Onderon</td><td><a href=".gitbook/assets/important_empire--onderon.png">important_empire--onderon.png</a></td><td><a href="empires/slice-region/theocracy-of-onderon.md">theocracy-of-onderon.md</a></td></tr><tr><td align="center">Sith Empire Remnants</td><td><a href=".gitbook/assets/important_empire--sith.png">important_empire--sith.png</a></td><td><a href="empires/hydian-borderlands/remnants-of-the-sith-empire.md">remnants-of-the-sith-empire.md</a></td></tr><tr><td align="center">Hutt Empire</td><td><a href=".gitbook/assets/important_empire--hutt.png">important_empire--hutt.png</a></td><td><a href="empires/slice-region/hutt-empire.md">hutt-empire.md</a></td></tr></tbody></table>
